package nl.bigo.kickit.android;

import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;
import com.google.android.gms.common.AccountPicker;
import com.squareup.otto.Subscribe;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;
import nl.bigo.kickit.android.event.*;
import nl.bigo.kickit.android.task.SendWebSocketMessageTask;
import nl.bigo.kickit.android.task.WebSocketServerConnectionTask;
import nl.bigo.model.Game;
import nl.bigo.model.Player;
import nl.bigo.model.WSEvent;

/**
 * The single activity that makes up this Android app.
 */
public class KickItActivity extends Activity {

  private static final String TAG = KickItActivity.class.getName();

  private WSClient wsClient = null;
  private Game game = null;
  private PlayerAdapter adapter = null;

  private static final String PREFERENCES = "PREFERENCES";
  private static final String KEY_ACCOUNT_NAME = "KEY_ACCOUNT_NAME";
  private static final int ACCOUNT_REQUEST_CODE = 123542;

  @Override
  public void onCreate(Bundle savedInstanceState) {

    super.onCreate(savedInstanceState);

    getActionBar().setDisplayShowTitleEnabled(false);

    setContentView(R.layout.activity_main);

    ListView list = (ListView) super.findViewById(R.id.player_list);

    adapter = new PlayerAdapter(this);
    list.setAdapter(adapter);

    super.findViewById(R.id.score_red).setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {

        String userName = getSetting(KEY_ACCOUNT_NAME);

        if (userName == null || game == null || !game.hasJoined(new Player(getSetting(KEY_ACCOUNT_NAME)))) {
          Toast.makeText(KickItActivity.this, R.string.Cannot_increment_score_login, Toast.LENGTH_LONG).show();
        }
        else if(game.hasJoined(new Player(userName))) {
          send(new WSEvent(WSEvent.Type.RED_SCORE), null);
        }
      }
    });

    super.findViewById(R.id.score_blue).setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {

        String userName = getSetting(KEY_ACCOUNT_NAME);

        if (userName == null || game == null || !game.hasJoined(new Player(userName))) {
          Toast.makeText(KickItActivity.this, R.string.Cannot_increment_score_login, Toast.LENGTH_LONG).show();
        }
        else if(game.hasJoined(new Player(userName))) {
          send(new WSEvent(WSEvent.Type.BLUE_SCORE), null);
        }
      }
    });

    super.findViewById(R.id.score_red).setOnLongClickListener(new View.OnLongClickListener() {
      @Override
      public boolean onLongClick(View v) {
        if (game == null || !game.hasJoined(new Player(getSetting(KEY_ACCOUNT_NAME)))) {
          Toast.makeText(KickItActivity.this, R.string.Cannot_undo_score_login, Toast.LENGTH_LONG).show();
        }
        else {
          send(new WSEvent(WSEvent.Type.RED_SCORE_UNDO), null);
        }
        return true;
      }
    });

    super.findViewById(R.id.score_blue).setOnLongClickListener(new View.OnLongClickListener() {
      @Override
      public boolean onLongClick(View v) {
        if (game == null || !game.hasJoined(new Player(getSetting(KEY_ACCOUNT_NAME)))) {
          Toast.makeText(KickItActivity.this, R.string.Cannot_undo_score_login, Toast.LENGTH_LONG).show();
        }
        else {
          send(new WSEvent(WSEvent.Type.BLUE_SCORE_UNDO), null);
        }
        return true;
      }
    });
  }

  @Override
  protected void onResume() {
    super.onResume();
    BusProvider.getBus().register(this);
    new WebSocketServerConnectionTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
  }

  @Override
  protected void onPause() {
    super.onPause();
    BusProvider.getBus().unregister(this);
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.action_menu, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case R.id.reconnect:
        new WebSocketServerConnectionTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        return true;
      case R.id.logout:
        String accountName = getSetting(KEY_ACCOUNT_NAME);
        if (accountName == null) {
          Toast.makeText(KickItActivity.this, getResources().getString(R.string.already_logged_out), Toast.LENGTH_LONG).show();
        }
        else {
          // A player can only leave when s/he hasn't joined a game that has already started.
          if(game != null && game.hasJoined(new Player(accountName)) && game.isStarted()) {
            Toast.makeText(KickItActivity.this, getResources().getString(R.string.cannot_logout), Toast.LENGTH_LONG).show();
          }
          else {
            removeSetting(KEY_ACCOUNT_NAME);
            Toast.makeText(KickItActivity.this, getResources().getString(R.string.logout_message, accountName), Toast.LENGTH_LONG).show();
            if(game != null && game.hasJoined(new Player(accountName))) {
              send(new WSEvent(WSEvent.Type.TOGGLE, new Player(accountName)), new ToastEvent(R.string.server_down));
            }
          }
        }
        return true;
      case R.id.help:
        Toast.makeText(KickItActivity.this, "With what?", Toast.LENGTH_LONG).show();
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
  }

  @Override
  protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {

    if (wsClient == null) {
      Toast.makeText(KickItActivity.this, R.string.server_down, Toast.LENGTH_LONG).show();
    }
    else if (resultCode == RESULT_OK && requestCode == ACCOUNT_REQUEST_CODE) {
      String accountName = data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
      saveSetting(KEY_ACCOUNT_NAME, accountName);
      send(new WSEvent(WSEvent.Type.TOGGLE, new Player(accountName)), new ToastEvent(R.string.server_down));
    }
  }

  private void send(WSEvent event, ToastEvent onFail) {

    Log.d(TAG, "sending: " + event);

    // First make sure the client is still properly connected.
    if (wsClient == null || !wsClient.isConnected()) {
      Log.d(TAG, "not connected anymore");
      new WebSocketServerConnectionTask(new RetrySendMessageEvent(event)).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }
    else {
      // Send the message.
      new SendWebSocketMessageTask(wsClient, event, onFail).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }
  }

  private String getSetting(String key) {
    SharedPreferences settings = getSharedPreferences(PREFERENCES, 0);
    return settings.getString(key, null);
  }

  private void removeSetting(String key) {
    this.saveSetting(key, null);
  }

  private void saveSetting(String key, String value) {
    SharedPreferences settings = getSharedPreferences(PREFERENCES, 0);
    SharedPreferences.Editor editor = settings.edit();
    editor.putString(key, value);
    editor.commit();
    Log.d(TAG, "saved setting " + key + " -> " + value);
  }

  private void login() {
    Intent intent = AccountPicker.newChooseAccountIntent(null, null, new String[]{"com.google"},
        false, null, null, null, null);
    startActivityForResult(intent, ACCOUNT_REQUEST_CODE);
  }

  @Subscribe
  public void onWSClientEvent(WebSocketClientConnectedEvent event) {

    this.wsClient = event.client;

    // After the connection was successfully made, ask the server for a list of joined players.
    new SendWebSocketMessageTask(this.wsClient, new WSEvent(WSEvent.Type.GAME))
        .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
  }

  @Subscribe
  public void onToastEvent(ToastEvent event) {
    Toast.makeText(this, event.stringResourceId, Toast.LENGTH_LONG).show();
  }

  @Subscribe
  public void onNewPlayerEvent(NewPlayerEvent event) {
    Toast.makeText(this, event.player, Toast.LENGTH_LONG).show();
  }

  @Subscribe
  public void onRetrySendMessageEvent(RetrySendMessageEvent event) {
    new SendWebSocketMessageTask(wsClient, event.event, new ToastEvent(R.string.server_down))
        .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
  }

  @Subscribe
  public void onRejectEvent(RejectEvent event) {
    if (event.message != null) {
      Toast.makeText(this, event.message, Toast.LENGTH_LONG).show();
    }
  }

  @Subscribe
  public void onGameEvent(GameEvent event) {

    this.game = event.game;

    Button button = (Button) findViewById(R.id.button);
    button.setEnabled(true);

    ((TextView) super.findViewById(R.id.score_red)).setText(String.valueOf(game.getScoreRed()));
    ((TextView) super.findViewById(R.id.score_blue)).setText(String.valueOf(game.getScoreBlue()));

    String accountName = getSetting(KEY_ACCOUNT_NAME);

    if (accountName != null && game.hasJoined(new Player(accountName))) {
      button.setText(R.string.leave);
    }
    else {
      button.setText(R.string.join);
    }

    button.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {

        String currentAccountName = getSetting(KEY_ACCOUNT_NAME);

        if (currentAccountName == null) {
          login();
        }
        else {
          send(new WSEvent(WSEvent.Type.TOGGLE, new Player(currentAccountName)), new ToastEvent(R.string.server_down));
        }
      }
    });

    adapter.setData(game.getPlayers());
    adapter.notifyDataSetChanged();

    if (game.getInfoMessage() != null) {
      Crouton.cancelAllCroutons();
      Crouton.makeText(this, game.getInfoMessage(), Style.INFO).show();
    }
  }

  @Subscribe
  public void onCountDownEvent(CountDownEvent event) {

    TextView view = (TextView) super.findViewById(R.id.txt_countdown);

    view.setText("");

    if(event.seconds >= 0) {

      int minutes = event.seconds / 60;
      int seconds = event.seconds % 60;

      view.setText(String.format("%d:%02d", minutes, seconds));
    }
  }
}

