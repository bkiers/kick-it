package nl.bigo.kickit.android;

import android.util.Log;
import com.google.gson.Gson;
import nl.bigo.kickit.android.event.*;
import nl.bigo.model.Game;
import nl.bigo.model.Team;
import nl.bigo.model.WSEvent;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.drafts.Draft_10;
import org.java_websocket.handshake.ServerHandshake;

import java.net.URI;

/**
 * The client that receives the events from the web socket server.
 */
public class WSClient extends WebSocketClient {

  private static final Gson GSON = new Gson();
  private static final String TAG = WSClient.class.getName();

  public WSClient(URI serverURI) {
    super(serverURI, new Draft_10());
  }

  @Override
  public void onOpen(ServerHandshake handshake) {
    Log.d(TAG, "opened connection");
  }

  @Override
  public void onMessage(String eventJson) {

    Log.d(TAG, "received: " + eventJson);

    WSEvent event = GSON.fromJson(eventJson, WSEvent.class);

    switch (event.type) {
      case GAME:
        BusProvider.getBus().post(new GameEvent(event.getTypeInstance(Game.class)));
        break;
      case REJECT:
        BusProvider.getBus().post(new RejectEvent(event.getTypeInstance(String.class)));
        break;
      case COUNT_DOWN:
        BusProvider.getBus().post(new CountDownEvent(event.getTypeInstance(Integer.class)));
        break;
      default:
        Log.d(TAG, "ignored event type: " + event.type);
        break;
    }
  }

  @Override
  public void onClose(int code, String reason, boolean remote) {
    Log.d(TAG, "connection closed by " + (remote ? "remote peer" : "us"));
  }

  @Override
  public void onError(Exception ex) {
    Log.d(TAG, "onError: " + ex.getCause());
  }

  public boolean isConnected() {
    return getConnection() != null && getConnection().isOpen();
  }
}
