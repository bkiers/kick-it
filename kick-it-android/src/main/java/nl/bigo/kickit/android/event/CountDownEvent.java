package nl.bigo.kickit.android.event;

/**
 * An event that is fired on the event bus by the server
 * with the new amount of seconds that are remaining before
 * the teams are divided.
 */
public class CountDownEvent {

  /**
   * The amount of seconds that are left before the teams are divided.
   */
  public final Integer seconds;

  public CountDownEvent(Integer seconds) {
    this.seconds = seconds;
  }
}
