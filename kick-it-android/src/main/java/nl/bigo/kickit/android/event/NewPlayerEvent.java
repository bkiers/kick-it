package nl.bigo.kickit.android.event;

/**
 * An event that is fired on the event bus when a new player
 * joined.
 */
public class NewPlayerEvent {

  /**
   * The email (uuid) of the new player.
   */
  public final String player;

  public NewPlayerEvent(String player) {
    this.player = player;
  }
}
