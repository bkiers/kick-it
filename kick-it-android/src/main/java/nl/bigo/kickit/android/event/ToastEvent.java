package nl.bigo.kickit.android.event;

/**
 * An event holding a reference to a message resource id
 * that should be picked up by an activity (or fragment)
 * that will display the massage as a context free toast.
 */
public class ToastEvent {

  public final int stringResourceId;

  public ToastEvent(int stringResourceId) {
    this.stringResourceId = stringResourceId;
  }
}
