package nl.bigo.kickit.android.event;

import nl.bigo.kickit.android.WSClient;

/**
 * An event that is fired on the event bus when a web-socket
 * connection is established.
 */
public class WebSocketClientConnectedEvent {

  /**
   * The client connection to the web-socket server.
   */
  public final WSClient client;

  public WebSocketClientConnectedEvent(WSClient client) {
    this.client = client;
  }
}
