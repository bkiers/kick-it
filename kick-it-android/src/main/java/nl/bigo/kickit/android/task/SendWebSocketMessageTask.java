package nl.bigo.kickit.android.task;

import android.os.AsyncTask;
import android.util.Log;
import com.google.gson.Gson;
import nl.bigo.kickit.android.WSClient;
import nl.bigo.kickit.android.event.BusProvider;
import nl.bigo.model.WSEvent;

/**
 * An async task responsible for sending a message to the
 * web-socket werver.
 */
public class SendWebSocketMessageTask extends AsyncTask<Void, Void, Boolean> {

  private static final String TAG = SendWebSocketMessageTask.class.getName();
  private static final Gson GSON = new Gson();

  // The web socket client connected to the server.
  private final WSClient client;

  // The event that needs to be sent to the server.
  private final WSEvent event;

  // An object that needs to be sent over the local event-bus in
  // case the event wasn't sent to the server successfully. In case
  // it is 'null', no event is sent over the local event-bus.
  private final Object onFail;

  /**
   * Creates a new SendWebSocketMessageTask.
   *
   * @param client
   *     the client that is connected to the server.
   * @param event
   *     the event that needs to be sent to the server.
   */
  public SendWebSocketMessageTask(WSClient client, WSEvent event) {
    this(client, event, null);
  }

  /**
   * Creates a new SendWebSocketMessageTask.
   *
   * @param client
   *     the client that is connected to the server.
   * @param event
   *     the event that needs to be sent to the server.
   * @param onFail
   *     in case this is not null, it will be sent over the
   *     local (Otto) bus in case the <code>event</code> was
   *     not sent to the server properly.
   */
  public SendWebSocketMessageTask(WSClient client, WSEvent event, Object onFail) {
    this.client = client;
    this.event = event;
    this.onFail = onFail;
  }

  @Override
  protected Boolean doInBackground(Void... params) {
    try {
      Log.i(TAG, "sending event: " + event);
      client.getConnection().send(GSON.toJson(event));
      return true;
    }
    catch (Exception e) {
      Log.e(TAG, "something went wrong sending a message to the WS server", e);
      return false;
    }
  }

  @Override
  protected void onPostExecute(Boolean success) {

    // Only post on the local event bus in case the 'event' wasn't sent
    // to the server _and_ 'onFail' was provided (not null).
    if (!success && onFail != null) {
      BusProvider.getBus().post(onFail);
    }
  }
}
