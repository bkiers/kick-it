package nl.bigo.model;

/**
 * A class holding some constant values.
 */
public final class Constants {

  // No need to instantiate this class.
  private Constants() {}

  /**
   * The amount of seconds before a user is removed from the list.
   */
  public static final int PLAYER_COUNT_DOWN = 1800;

  /**
   * The hostname of the web socket server.
   */
  public static final String HOST = "big-o.nl";

  /**
   * The port of the web socket server.
   */
  public static final int PORT = 8886;
}
