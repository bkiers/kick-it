package nl.bigo.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * A representation of a game keeping track of the players and score.
 */
public class Game {

  private final int totalPlayers;
  private final int winningScore;

  private List<Player> players;
  private int scoreRed;
  private int scoreBlue;
  private boolean started;
  private String infoMessage;

  public Game() {
    this(4, 10);
  }

  public Game(int totalPlayers, int winningScore) {
    this.totalPlayers = totalPlayers;
    this.winningScore = winningScore;
    this.clear();
  }

  public void clear() {
    this.players = new ArrayList<Player>();
    this.scoreRed = 0;
    this.scoreBlue = 0;
    this.started = false;
    this.infoMessage = null;
  }

  protected void addPlayer(Player player) {

    if(started) {
      throw new RuntimeException("Cannot join this game: it has already started!");
    }

    if (!players.contains(player)) {
      players.add(player);
      infoMessage = player + " would like to play a game!";
    }
  }

  public Player getPlayer(String email) {
    Player temp = new Player(email);
    int index = players.indexOf(temp);
    if(index < 0) {
      return null;
    }
    return players.get(index);
  }

  public boolean hasWinner() {
    return this.getWinningTeam() != Team.UNKNOWN;
  }

  public boolean hasSufficientPlayers() {
    return players.size() >= totalPlayers;
  }

  public void makeTeams() {

    List<Team> teamList = new ArrayList<Team>();

    int half = totalPlayers / 2;

    for(int i = 0; i < half; i++) {
      teamList.add(Team.BLUE);
    }

    for(int i = 0; i < half; i++) {
      teamList.add(Team.RED);
    }

    for(int i = totalPlayers; i < players.size(); i++) {
      teamList.add(Team.UNKNOWN);
    }

    Collections.shuffle(teamList);

    for(int i = 0; i < players.size(); i++) {
      players.get(i).setTeam(teamList.get(i));
    }

    infoMessage = "Teams are made, let the game begin!";
    started = true;
  }

  protected void removePlayer(Player p) {

    if (!players.contains(p)) {
      return;
    }

    Player player = players.get(players.indexOf(p));

    if(started && player.getTeam() != Team.UNKNOWN) {
      throw new RuntimeException("Cannot leave this game: it has already started!");
    }

    players.remove(player);
    infoMessage = player + " left the game";
  }

  public void toggle(Player player) {
    if(players.contains(player)) {
      removePlayer(player);
    }
    else {
      addPlayer(player);
    }
  }

  public void incrementBlueScore() {
    if(!started) {
      throw new RuntimeException("Cannot increment the score: the game hasn't begun");
    }
    if(scoreRed >= winningScore) {
      throw new RuntimeException("Cannot increment the score: red already has " + winningScore + " goals");
    }
    if(scoreBlue >= winningScore) {
      throw new RuntimeException("Cannot increment the score: blue already has " + winningScore + " goals");
    }
    infoMessage = "Blue scored!";
    scoreBlue++;
  }

  public void incrementRedScore() {
    if(!started) {
      throw new RuntimeException("Cannot increment the score: the game hasn't begun");
    }
    if(scoreRed >= winningScore) {
      throw new RuntimeException("Cannot increment the score: red already has " + winningScore + " goals");
    }
    if(scoreBlue >= winningScore) {
      throw new RuntimeException("Cannot increment the score: blue already has " + winningScore + " goals");
    }
    infoMessage = "Red scored!";
    scoreRed++;
  }

  public void incrementBlueScoreUndo() {
    if(!started) {
      throw new RuntimeException("Cannot undo the score: the game hasn't begun");
    }
    if(scoreBlue <= 0) {
      throw new RuntimeException("Cannot increment the score: blue already has 0 goals");
    }
    infoMessage = "Apparently no score for blue...";
    scoreBlue--;
  }

  public void incrementRedScoreUndo() {
    if(!started) {
      throw new RuntimeException("Cannot undo the score: the game hasn't begun");
    }
    if(scoreRed <= 0) {
      throw new RuntimeException("Cannot increment the score: red already has 0 goals");
    }
    infoMessage = "Apparently no score for red...";
    scoreRed--;
  }

  public Team getWinningTeam() {
    if(scoreBlue >= winningScore) {
      infoMessage = "The blue team has won!";
      return Team.BLUE;
    }
    else if(scoreRed >= winningScore) {
      infoMessage = "The red team has won!";
      return Team.RED;
    }
    else {
      return Team.UNKNOWN;
    }
  }

  public String getInfoMessage() {
    return infoMessage;
  }

  public boolean hasJoined(Player player) {
    return players.contains(player);
  }

  public int getTotalPlayers() {
    return totalPlayers;
  }

  public int getWinningScore() {
    return winningScore;
  }

  public List<Player> getPlayers() {
    return players;
  }

  public int getScoreRed() {
    return scoreRed;
  }

  public int getScoreBlue() {
    return scoreBlue;
  }

  public boolean isStarted() {
    return started;
  }

  public void resetInfoMessage() {
    this.infoMessage = null;
  }

  @Override
  public String toString() {
    return "Game{" +
        "totalPlayers=" + totalPlayers +
        ", winningScore=" + winningScore +
        ", players=" + players +
        ", scoreRed=" + scoreRed +
        ", scoreBlue=" + scoreBlue +
        ", started=" + started +
        ", infoMessage='" + infoMessage + '\'' +
        '}';
  }
}
