package nl.bigo.model;

/**
 * A table soccer team.
 */
public enum Team {

  RED, BLUE, UNKNOWN;

  public static Team opposite(Team team) {
    switch (team) {
      case RED:
        return BLUE;
      case BLUE:
        return RED;
      default:
        return UNKNOWN;
    }
  }
}
