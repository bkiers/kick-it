package nl.bigo.model;

import com.google.gson.Gson;

/**
 * An event that can be sent from the web socket client and server.
 */
public class WSEvent {

  /**
   * The type of an event.
   */
  public static enum Type {

    /** A join- or leave action of the player. */
    TOGGLE,

    /** Sent to the player when the server rejects an action. */
    REJECT,

    /** Indicating that the event contains the game state. */
    GAME,

    /** Indicates a score from the red team. */
    RED_SCORE,

    /** Indicates a score from the blue team. */
    BLUE_SCORE,

    /** Indicates a reversal of a score from the red team. */
    RED_SCORE_UNDO,

    /** Indicates a reversal of a score from the blue team. */
    BLUE_SCORE_UNDO,

    /** Indicates a new count down number. */
    COUNT_DOWN
  }

  private static final Gson GSON = new Gson();

  /**
   * The type of the event.
   */
  public final Type type;

  /**
   * The payload of the event as a JSON string.
   */
  public final String json;

  /**
   * Creates a new event without a payload.
   *
   * @param type
   *     the type of the event.
   */
  public WSEvent(Type type) {
    this(type, null);
  }

  /**
   * Creates a new event with a payload.
   *
   * @param type
   *     the type of the event.
   * @param object
   *     the payload of the event which will be converted into
   *     a JSON string.
   */
  public WSEvent(Type type, Object object) {
    this.type = type;
    this.json = GSON.toJson(object);
  }

  /**
   * Returns the actual instance from the payload attached to this
   * event.
   *
   * @param clazz
   *     the class of the instance.
   * @param <T>
   *     the generic type of the instance.
   *
   * @return the actual instance from the payload attached to this
   *         event.
   */
  public <T> T getTypeInstance(Class<T> clazz) {
    try {
      return GSON.fromJson(json, clazz);
    }
    catch(Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  @Override
  public String toString() {
    return "WSEvent{" +
        "type=" + type +
        ", json='" + json + '\'' +
        '}';
  }
}
